"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var aedes = require('aedes');

var logging = require('aedes-logging');

var fs = require('fs');

var tls = require('tls');

var aedesPersistenceMongoDB = require('aedes-persistence-mongodb');

var mqmongo = require('mqemitter-mongodb');

var BrokerService = /*#__PURE__*/function () {
  function BrokerService() {
    _classCallCheck(this, BrokerService);

    var persistence = aedesPersistenceMongoDB({
      url: 'mongodb://127.0.0.1/tefipro_broker',
      // ttl: 8640000 // 100 días,
      ttl: 300 // 5 minutos

    });
    var emitter = mqmongo({
      url: 'mongodb://127.0.0.1/tefipro_broker'
    });
    this.serverOptions = {
      key: fs.readFileSync(__dirname + '/../..' + process.env.SSL_KEY),
      cert: fs.readFileSync(__dirname + '/../..' + process.env.SSL_CERT)
    };
    this.aedesSettings = {
      mq: emitter,
      persistence: persistence
    };
    this.init();
  }

  _createClass(BrokerService, [{
    key: "init",
    value: function init() {
      var broker = new aedes.Server(this.aedesSettings);
      var server = tls.createServer(this.serverOptions, broker.handle);

      var log = function log(text) {
        console.log("[".concat(new Date().toLocaleString(), "] ").concat(text));
      };

      logging({
        instance: broker,
        server: server
      });
      broker.on('client', function (client) {
        var message = "Client ".concat(client.id, " just connected");
        log(message);
      });
      broker.on('clientDisconnect', function (client) {
        var message = "Client ".concat(client.id, " just DISconnected");
        log(message);
      });
      server.listen(process.env.BROKER_PORT, function () {
        console.log('MQTTS server broker started and listening on port ', process.env.BROKER_PORT);
      });
    }
  }]);

  return BrokerService;
}();

exports["default"] = BrokerService;