"use strict";

var _broker = _interopRequireDefault(require("./services/broker.service"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

require('custom-env').env();

new _broker["default"]();