## Description

NestJS project for adidas weather app challenge.

This repository is part of the "adidas weather app" challenge. To see all project repositories go to https://bitbucket.org/codekstudio/workspace/projects/ADI

The mongo database engine must be correctly installed and running on the local machine so that the app can be record, erase and retrieve logs data. The name of the database will be adidas_broker.

This repository is the broker mqtt of the system and comprises a simple message logging and distribution service based on the mqtt protocol.


## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start:dev

# production mode
$ npm run start
```


## To be done

All the work here is finished, but right now, only the backend is connecting as a client. Starting both repositories (first the broker and then the backend) it should be seen in the console that the connection has been established:

In the borker you will see:

```bash
Client adidas-backend-1bff2ec5 just connected
```

and in the backend:

```bash
--------- Entering messsage ----------
{"cmd":"publish","retain":false,"qos":0,"dup":false,"length":34,"topic":"adidas/core/backend/status","payload":{"type":"Buffer","data":[79,110,108,105,110,101]}}
adidas/core/backend/status
Online
```