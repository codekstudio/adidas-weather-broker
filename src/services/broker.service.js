const aedes = require('aedes');
const logging = require('aedes-logging');
const fs = require('fs')
const tls = require('tls');
const aedesPersistenceMongoDB = require('aedes-persistence-mongodb');
const mqmongo = require('mqemitter-mongodb');

export default class BrokerService {

    constructor() {
        const persistence = aedesPersistenceMongoDB({
            url: 'mongodb://127.0.0.1/adidas_broker',
            // ttl: 8640000 // 100 días,
            ttl: 300 // 5 minutos
        });

        const emitter = mqmongo({
            url: 'mongodb://127.0.0.1/adidas_broker'
        });
        
        this.serverOptions = {
            key: fs.readFileSync(__dirname + '/../..' + process.env.SSL_KEY),
            cert: fs.readFileSync(__dirname + '/../..' + process.env.SSL_CERT)
        }

        this.aedesSettings = {
            mq: emitter,
            persistence: persistence
        }

        this.init(); 
    }

    init() {
        const broker = new aedes.Server(this.aedesSettings);
        const server = tls.createServer(this.serverOptions, broker.handle);

        const log = (text) => {
            console.log(`[${new Date().toLocaleString()}] ${text}`)
        }
          
        logging({
            instance: broker,
            server: server
        })
        
        broker.on('client', (client) => {
            let message = `Client ${client.id} just connected`
            log(message)
        })
        
        broker.on('clientDisconnect', (client) => {
            let message = `Client ${client.id} just DISconnected`
            log(message)
        })
        
        
        server.listen(process.env.BROKER_PORT, function () {
            console.log('MQTTS server broker started and listening on port ', process.env.BROKER_PORT)
        })
    }
    
}